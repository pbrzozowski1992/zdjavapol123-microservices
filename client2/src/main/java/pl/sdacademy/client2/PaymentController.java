package pl.sdacademy.client2;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RequestMapping("/payment")
@RestController
public class PaymentController {

    private final RestTemplate restTemplate;

    public PaymentController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping
    public String paymentDetails() {
        String apiKey = restTemplate.getForObject(
                "http://config-service/config", String.class
        );
        return "Payment details: " + apiKey;
    }

    @GetMapping("/welcome")
    public String welcomeUser() {
        String login = restTemplate.getForObject("http://config-service/config/login", String.class);
        return "Welcome " + login + "!";
    }

    @GetMapping("/guess/{value}")
    public String guessNumber(@PathVariable int value) {
        Integer number = restTemplate.getForObject("http://config-service/config/number", Integer.class);
        if (number == value) {
            return "You win!";
        } else {
            return "distance between your value and result was: " + Math.abs(number - value);
        }
    }
}

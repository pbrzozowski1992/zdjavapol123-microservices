package pl.sdacademy.client2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RequestMapping("/bank")
@RestController
public class BankAccountController {

    private final RestTemplate restTemplate;


    @Autowired
    public BankAccountController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping
    public String getUserBankAccount(@RequestParam String login,
                                     @RequestParam String password) {
        User user = restTemplate.getForObject(
                String.format("http://config-service/user?login=%s&password=%s", login, password),
                User.class);
        return user.getBankAccount();
    }

}

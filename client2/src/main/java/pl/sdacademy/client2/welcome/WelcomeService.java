package pl.sdacademy.client2.welcome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WelcomeService {

    private final RestTemplate restTemplate;

    @Autowired
    public WelcomeService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getWelcomeMessage(String login, String password) {
        try {
            String sessionId = restTemplate.getForObject(
                    String.format("http://config-service/session?login=%s&password=%s", login, password),
                    String.class);
            return "Welcome " + sessionId;
        } catch (Exception e) {
            return "Can't establish session!";
        }
    }
}

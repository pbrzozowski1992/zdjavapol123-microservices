package pl.sdacademy.client2;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RequestMapping("/lottoclient")
@RestController
public class LottoClientController {

    private final RestTemplate restTemplate;

    public LottoClientController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/{type}")
    public boolean checkResults(@PathVariable String type,
                                @RequestParam List<Integer> numbers){
        LottoResults lottoResults = restTemplate.getForObject(
                "http://config-service/lotto",
                LottoResults.class
        );
        Integer[] results;
        if (type.equals("multi")){
            results = lottoResults.getMultiLotto();
        } else {
            results = lottoResults.getBigLotto();
        }

        int score = 0;
        for (int number : numbers) {
            for (int result : results) {
                if (number == result) {
                    score++;
                }
            }
        }
        return score >= 3;
    }
}

class LottoResults {

    private Integer[] multiLotto;
    private Integer[] bigLotto;

    public Integer[] getMultiLotto() {
        return multiLotto;
    }

    public void setMultiLotto(Integer[] multiLotto) {
        this.multiLotto = multiLotto;
    }

    public Integer[] getBigLotto() {
        return bigLotto;
    }

    public void setBigLotto(Integer[] bigLotto) {
        this.bigLotto = bigLotto;
    }
}

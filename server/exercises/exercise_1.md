# Zadanie 1

## Zadanie podstawowe

* W mikroserwisie `client1` napisz usługę, która wylosuje jedną liczbę z przedziału 0-49
* W mikroserwisie `client2` napisz usługę, która pobierze od użytkownika jedną liczbę (request param, path varaible) i sprawdzi czy podana liczba jest równa wylosowanej. Jeśli nie zostanie zwrócna informacja jak bardzo użytkownik się pomylił.

## Zadanie dodatkowe

* W mikroserwisie `client1` napisz usługę która zwróci 6 wylosowanych liczb
* W mikroserwisie `client2` napisz usługę, która sprawdzi czy użytkownik trafił 6 w dużego lotka.


# Zadanie 2

## Zadanie podstawowe

* W mikroserwisie `client1` napisz usługę, która zwróci nam obiekt z rezultatami losowań lott:

```java
class Lotto {
    private Integer[] multiLotto;
    private Integer[] bigLotto;
}
```

- Stwórz nowy controller -> LottoController
- Napisz metodę GET zwróci rezultaty losowania

* w mikroserwisie `client2` napisz usugłe, która w zależności od typu losowania pobierze wyniki i sprawdzi czy wygrałeś
  (liczba trafień >= 3 -> wygrana)
- Stwórz nowy controller -> LottoClientController
- Napisz metodę GET która przyjmie jako parametr nasze skreślone liczy oraz typ losowania
- metoda musi pobrać z `client1` wyniki losowan i porównać z naszymi skreśleniami


## Zadanie dodatkowe

* zmodyfikuj usugę w `client1` tak by zwracała wyniki losowania przekazanego jako parametr żądania, np.
* `client2` musi określi odpowiednią wygraną
  Multilotto -> 7 trafień 10000 wygranej, 8 trafień 20000 wygranej, 9 trafień 30000
  Duże Lotto -> 3 trafienia 10zł, 4, trafienia 100zł, 5 trafień 1000 zł, 6 trafień 100000
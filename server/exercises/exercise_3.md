# Zadanie 3

## Client1
* w `client1` napisz usługę, która sprawdzi, czy istnieje użytkownik o danym loginie i haśle (parametry powinny być przekazane jako @RequestParam)
* jeśli użytkownik istnieje powinien zostać zwrócny losowy ciąg znaków (np. UUID) sesja. Sesja powinna być polem w klasie User
* jeśli sesja jest przypisana do użytkownika to powinna zostać zwrócona istniejąca wartość, jeśli nie powinna zostać wygenerowana i przypisana do użytkownika


```java
class User {
    private String login;
    private String password;
    private String sessionId;
}
```

```java
private final List<User> users = Arrays.asList(new User("login", "password"), new User("login1", "password1"));
```

```java
if(sessionId == null) {
    String sessionId = //generate value
    user.sessionId = sessionId;  
    return sessionId;
} else {    
    return user.sessionId;
}
```

Kroki:
* stwórz klasę User
* stwórz serwis (do obłsugi sesji)
* serwis powinien zawierać listę użytkowników i udostępniać metodę `getUserSession`
* stwórz controller (typ REST, mapping: /users)
* wstrzyknij serwis w klasie controller
* napisz metodę GET w kontrollerze, która wywoła serwis w celu sprawdzenia czy użytkownik istnieje i wygenerowania/zwrócenia sesji


## Client2

* w ramach `client2` napisz usługę, która wyświetli komunikat powitalny dla użytkownika w raz z zwróconym sessionId

Kroki:

* stwórz WelcomeService
* wstrzyknij `RestTemplate`
* napisz metodę, która odpyta mikroserwis1 ("http://config-service/session") i pobierze sesję użytkownika
* jeśli sesja istnieje metoda powinna zwrócić komunikat powitalny
* w przypadku błędu pobrania sesji wyświetl stosowany komunikat (użyj try-catch przy wowłaniu rest template)
* stwórz `WelcomeController` (typ: REST, mapping: "/welcome")
* wstrzykinj `WeclomeService`
* napisz metodę GET która przyjmie login i hasło użytkownika (parametry żadania) i wywoła serwis w celu zwrócenia komunikatu
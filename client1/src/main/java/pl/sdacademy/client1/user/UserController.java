package pl.sdacademy.client1.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/user")
@RestController
public class UserController {

    private final List<User> users = Arrays.asList(
            new User("test", "test"),
            new User("test1", "test1")
    );

    @GetMapping
    public User getUserByCredentials(@RequestParam String login,
                                     @RequestParam String password) {
        return users.stream().filter(u -> u.getLogin().equals(login) && u.getPassword().equals(password))
                .findFirst().orElseThrow();
    }
}

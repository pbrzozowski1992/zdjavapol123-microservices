package pl.sdacademy.client1.user;

import java.util.UUID;

public class User {
    private String login;
    private String password;

    private String bankAccount;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
        this.bankAccount = UUID.randomUUID().toString();
    }

    public User() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }
}

package pl.sdacademy.client1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;
import java.util.UUID;

@RequestMapping("/config")
@RestController
public class ConfigController {

    @GetMapping
    public String getApiKey() {
        return UUID.randomUUID().toString();
    }

    @GetMapping("/login")
    public String getLogin() {
        return "login@sda.pl";
    }

    @GetMapping("/number")
    public Integer getNumber() {
        return new Random().nextInt(0, 50);
    }
}

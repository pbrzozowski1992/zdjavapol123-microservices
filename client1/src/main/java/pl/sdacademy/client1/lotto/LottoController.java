package pl.sdacademy.client1.lotto;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RequestMapping("/lotto")
@RestController
public class LottoController {

    private final Lotto lottoResults;

    public LottoController() {
        Random random = new Random();
        Integer multiLotto[] = new Integer[20];
        for (int i=0;i<20;i++){
            multiLotto[i] = random.nextInt(0,100);
        }
        Integer bigLotto[] = new Integer[6];
        for (int i=0;i<6;i++){
            bigLotto[i] = random.nextInt(0,50);
        }
        lottoResults = new Lotto(multiLotto, bigLotto);
    }

    @GetMapping
    public Lotto getLottoResults() {
        return lottoResults;
    }
}

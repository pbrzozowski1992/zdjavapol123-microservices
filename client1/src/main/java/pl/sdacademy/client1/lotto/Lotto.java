package pl.sdacademy.client1.lotto;

class Lotto {
    private Integer[] multiLotto;
    private Integer[] bigLotto;

    public Lotto() {
    }

    public Lotto(Integer[] multiLotto, Integer[] bigLotto) {
        this.multiLotto = multiLotto;
        this.bigLotto = bigLotto;
    }

    public Integer[] getMultiLotto() {
        return multiLotto;
    }

    public void setMultiLotto(Integer[] multiLotto) {
        this.multiLotto = multiLotto;
    }

    public Integer[] getBigLotto() {
        return bigLotto;
    }

    public void setBigLotto(Integer[] bigLotto) {
        this.bigLotto = bigLotto;
    }
}
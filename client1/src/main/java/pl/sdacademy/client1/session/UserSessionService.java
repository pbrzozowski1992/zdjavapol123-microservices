package pl.sdacademy.client1.session;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class UserSessionService {

    private final List<User> users = Arrays.asList(
            new User("login1", "password1"),
            new User("login2", "password2")
    );

    public String getUserSession(String login, String password) {
        User user = users.stream().filter(u->u.getLogin().equals(login) && u.getPassword().equals(password))
                .findFirst().orElseThrow();

        if (user.getSessionId() == null) {
            String sessionId = UUID.randomUUID().toString();
            user.setSessionId(sessionId);
            return sessionId;
        } else {
            return user.getSessionId();
        }
    }

}
